const { pause } = require("codeceptjs");

Funcionalidade('login');

Cenário('Login com sucesso',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
//    pause();
    I.fillField({ id: 'user' },'android_17@teste.com')
    I.fillField({ id: 'password' },'Qazando01')
    I.click({ id: 'btnLogin' })
    I.see('Olá, android_17@teste.com')

}).tag('@positivo')

Cenário('Login Falha - Informar Apenas E-mail',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField({ id: 'user' },'android_17@teste.com')
    I.click({ id: 'btnLogin' })
    I.see('Senha inválida.')

}).tag('@negativo')

Cenário('Login Falha - Senha Incorreta',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField({ id: 'user' },'android_17@teste.com')
    I.click({ id: 'btnLogin' })
    I.see('Senha inválida.')

}).tag('@negativo')

Cenário('Login Falha - Não Informar Dados de Acesso',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.click({ id: 'btnLogin' })
    I.see('E-mail inválido.')
  //  pause();

}).tag('@negativo')


